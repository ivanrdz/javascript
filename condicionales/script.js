const starWars7 = 'Star Wars: El despertar de la fuerza'
const pgStarWars7 = 13

const nameSacha = 'Sacha'
const ageSacha = 26

const nameSanti = 'santi'
const ageSanti = 12

function canWatchSatarWars7(name, age, isWithAdult = false){
	if (age >= pgStarWars7){
		alert(`${name} puede pasar a ver ${starWars7}`)
	}else if(isWithAdult){
		alert(`${name} puede pasar a ver ${starWars7}. Aun que su edad es ${age}, se encuentra acompañado por un adulto`)
	}else{
		alert(`${name} no puede pasar a ver ${starWars7} tiene ${age} años y necesita tener ${pgStarWars7}`)
	}
}

canWatchSatarWars7(nameSacha, ageSacha)
canWatchSatarWars7(nameSanti, ageSanti, true)